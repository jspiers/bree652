![Presentation Title Slide](presentation.jpg)

### Dependencies
- [python3](https://www.python.org/)
- [PyTorch](https://pytorch.org/) with CUDA support
- [tqdm](https://github.com/tqdm/tqdm)

