#!/usr/bin/env python3

# Standard imports
import sys
import os
import logging
from shutil import copyfile
from io import BytesIO
from multiprocessing import Pool
from itertools import count

# Library imports
import torch
from torch.nn.functional import conv1d
from torchvision.transforms.functional import to_pil_image, to_tensor
import numpy as np
from PIL import Image
from tqdm import trange, tqdm


def mkdir_like(hint="frames"):
    " Create and return directory with name similar (or identical) to 'hint' "
    dirname = hint
    suffix = 1
    while os.path.exists(dirname):
        dirname = "{}_{:d}".format(hint, suffix)
        suffix += 1
    os.makedirs(dirname)
    return dirname


def forloop(image):
    return [[min(4 * abs(next_pixel - pixel), 255)
            for pixel, next_pixel in zip(row, row[1:])]
            for row in image]


def torchedge(image):
    kernel = image.new_tensor([[[[-1, 1]]]])
    return torch.clamp(4 * conv1d(image, kernel).abs(), 0, 255)


def main(verbosity, imagedir, verify, stack):
    logging.basicConfig(level=logging.DEBUG
                        if verbosity
                        else logging.INFO, format="%(message)s")

    # Let's not waste our time finding out only later that CUDA isn't available
    assert torch.cuda.is_available()

    imagedir = imagedir.rstrip('/')
    assert os.path.isdir(imagedir)
    outdir = mkdir_like(imagedir + "-edges")

    imagefilenames = []
    for fn in sorted(os.listdir(imagedir)):
        if fn.endswith(".jpg"):
            imagefilenames.append(os.path.join(imagedir, fn))
        else:
            src = os.path.join(imagedir, fn)
            assert not os.path.isdir(src)
            dest = os.path.join(outdir, fn)
            logging.info("Copying non-image file %r to %r", src, dest)
            copyfile(src, dest)

    logging.info("Loading images as NumPy arrays")
    npimages = np.stack([make_npimage(fn) for fn in imagefilenames])
    logging.debug("npimages.shape %r", npimages.shape)

    edgeimages = list(process_images(npimages, verify))

    logging.info("Saving edge images to %r", outdir)
    for edgeimage, imagefilename in zip(edgeimages, imagefilenames):
        outfilename = os.path.join(outdir, os.path.basename(imagefilename))
        if stack:
            # Stack original and edge image
            # (truncating first column of original to match dimensions)
            orig = make_npimage(imagefilename, grayscale=False)[:, 1:]

            # Convert gray edge image to color by replicating
            # single gray channel as three RGB channels
            edgeimage = np.stack(3 * [edgeimage], axis=-1)

            # Stack original and edge image
            outimage = np.vstack((orig, edgeimage))
        else:
            outimage = edgeimage
        to_pil_image(outimage.astype(np.uint8)).save(outfilename)


def make_npimage(imagefilename, grayscale=True, crop=None):
    # Open image
    image = Image.open(imagefilename)
    logging.debug("%s", image)
    width, height = image.size
    logging.debug("width %d height %d", width, height)

    # Grayscale
    if grayscale:
        logging.debug("Converting to grayscale")
        image = image.convert("L")

    if crop is not None:
        logging.debug("Cropping to %r", crop)
        image = image.crop((0, 0, *crop))

    # Read the image bitmap into numpy array
    return np.array(image, dtype=int)


def make_listimage(npimage):
    return [[int(pixel) for pixel in row] for row in npimage]


def sanitycheck(npimages, listimages):
    for npimage, listimage in zip(npimages, listimages):
        for i in (0, -1):
            for j in (0, -1):
                assert listimage[i][j] == npimage[i][j], (i, j)
                logging.debug("listimage[%d][%d]: %r", i, j, listimage[i][j])
                logging.debug("npimage[%d][%d]: %r", i, j, npimage[i][j])


def load_images(npimages):
    logging.info("Converting NumPy arrays to lists of lists")
    listimages = [make_listimage(npimg)
                  for npimg in tqdm(npimages, total=npimages.shape[0])]
    logging.info("Sanity checking")
    sanitycheck(npimages, listimages)
    logging.info("Converting NumPy arrays to PyTorch Tensor")
    tensorcpu = torch.from_numpy(npimages).unsqueeze(1)
    logging.debug("tensorcpu.shape %r", tensorcpu.shape)
    return listimages, tensorcpu


def process_images(npimages, verify):

    listimages, tensor = load_images(npimages)

    # Run the edge-detection algorithms
    algocount = count(1) # keep track of algorithm number
    edges = [] # to accumulate edgemap results

    logging.info("Algorithm #%d:  Pixel-by-Pixel (1 CPU)", next(algocount))
    for listimage in tqdm(listimages, unit="frames"):
        edges_single = forloop(listimage)
    edges.append(edges_single)

    logging.info("Algorithm #%d:  Multiprocessing (%d CPUs)",
                 next(algocount), os.cpu_count())
    with Pool() as pool, tqdm(total=len(listimages), unit="frames") as t:
        results = [pool.apply_async(forloop, (listimage,), callback=lambda x: t.update())
                   for listimage in listimages]
        edges_mp = [r.get() for r in results]
    edges.append(edges_mp[-1])

    if not verify:
        # Release memory
        del listimages

    logging.info("Algorithm #%d:  PyTorch Convolution (CPU)", next(algocount))
    edgebatches = []
    with tqdm(total=tensor.shape[0], unit="frames") as t:
        for batch in torch.split(tensor, 30):
            edgebatches.append(torchedge(batch))
            t.update(batch.shape[0])
    edges.append(edgebatches[-1][-1, :, :, :].squeeze())

    tensor = tensor.to(torch.device("cuda"), dtype=torch.float)
    logging.info("Algorithm #%d:  PyTorch Convolution (GPU)", next(algocount))
    with tqdm(total=tensor.shape[0], unit="frames") as t:
        for batch in torch.split(tensor, 30):
            edges_gpu = torchedge(batch)
            t.update(batch.shape[0])
    edges.append(edges_gpu[-1, :, :, :].squeeze().cpu())

    if verify:
        # Convert all to list of lists
        logging.info("Converting results to list of lists for verification...")
        for i, e in enumerate(edges):
            if type(e) is list:
                logging.debug("list of %d lists of %d elements",
                              len(e), len(e[0]))
            else:
                logging.debug("type %r shape %r", type(e), e.shape)
                edges[i] = [[int(pixel) for pixel in row] for row in e]

        # Verify that results match
        logging.info("Verifying that results match for all algorithms...")
        for i, edges_ in enumerate(edges[1:], 2):
            assert edges[0] == edges_,\
                   "Algorithm {} edges != Algorithm 1 edges".format(i)

    # Convert edges to Image and save
    for batch in edgebatches:
        for edgearray in batch.squeeze():
            yield edgearray.numpy()


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="for every pixel")
    parser.add_argument("-v", "--verbosity", action="count", default=0)
    parser.add_argument("--verify", action="store_true")
    parser.add_argument("--stack", action="store_true")
    parser.add_argument("imagedir")
    kwargs = vars(parser.parse_args())
    sys.exit(main(**kwargs))
